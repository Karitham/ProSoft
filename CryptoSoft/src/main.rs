use std::io;
extern crate clap;
use clap::{App, Arg, SubCommand};
use crypto_soft::railfence::RailFence;

fn main() -> io::Result<()> {
    let mut stdin = io::stdin();
    let mut stdout = io::stdout();

    let matches = App::new("crypto_soft")
        .version("0.1.0")
        .author("Pierre-Louis Pery")
        .about("Encode and decode files with the rail-fence cypher")
        .arg(
            Arg::with_name("width")
                .short("w")
                .long("width")
                .value_name("width")
                .takes_value(true)
                .help("The width of the rail-fence cypher"),
        )
        .subcommand(SubCommand::with_name("encode").about("Encode a file"))
        .subcommand(SubCommand::with_name("decode").about("Decode a file"))
        .get_matches();

    let width = if let Some(w) = matches.value_of("width") {
        w.parse::<usize>().unwrap()
    } else {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "No width specified",
        ));
    };

    match matches.subcommand() {
        ("encode", Some(_)) => encode(width, &mut stdin, &mut stdout)?,
        ("decode", Some(_)) => decode(width, &mut stdin, &mut stdout)?,
        _ => println!("{}", matches.usage()),
    }

    Ok(())
}

fn encode(width: usize, reader: &mut dyn io::Read, writer: &mut dyn io::Write) -> io::Result<()> {
    let rf = RailFence::new(width);

    let mut line = vec![];
    reader.read_to_end(&mut line)?;

    let out = rf.encode(line.into_iter());
    writer.write(&out)?;

    Ok(())
}

fn decode(width: usize, reader: &mut dyn io::Read, writer: &mut dyn io::Write) -> io::Result<()> {
    let r = RailFence::new(width);

    let mut line = vec![];
    reader.read_to_end(&mut line)?;

    let out = r.decode(&line).unwrap();
    writer.write(&out)?;

    Ok(())
}

// an iterator which yields values that swing between two limits
pub struct Pendulum {
    pub val: usize,
    pub lower: usize,
    pub upper: usize,
    pub step: isize,
}

impl Pendulum {
    pub fn new(upper: usize) -> Self {
        Pendulum {
            val: 0,
            lower: 0,
            upper,
            step: 1,
        }
    }
}

impl Iterator for Pendulum {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        let old_val = self.val;

        self.val = (self.val as isize + self.step) as usize;
        if self.val == self.lower {
            self.step = 1;
        }

        if self.val == self.upper {
            self.step = -1;
        }

        Some(old_val)
    }
}

use crate::pendulum::Pendulum;
extern crate test;

pub struct RailFence {
    rails: usize,
}
impl RailFence {
    pub fn new(rails: usize) -> RailFence {
        RailFence { rails }
    }

    pub fn encode(&self, text: impl Iterator<Item = u8>) -> Vec<u8> {
        let rails = self.rails;
        let mut str_rails = vec![vec![]; rails];

        text.zip(Pendulum::new(rails - 1))
            .for_each(|(c, i)| str_rails[i].push(c));

        str_rails.into_iter().flatten().collect()
    }

    fn lines(&self, len: usize) -> Vec<usize> {
        let mut line_sizes = vec![0usize; self.rails];
        for i in Pendulum::new(self.rails - 1).take(len) {
            line_sizes[i] += 1;
        }
        line_sizes
    }

    fn decompose_cipher(&self, cipher: &[u8]) -> Vec<Vec<u8>> {
        let l_sizes = self.lines(cipher.len());
        let mut chars = cipher.iter();

        let mut out: Vec<Vec<u8>> = vec![];

        for size in l_sizes {
            let mut rail = vec![];
            for _ in 0..size {
                rail.push(*chars.next().expect("sum of size == count"));
            }

            rail.reverse();
            out.push(rail);
        }
        out
    }

    pub fn decode(&self, cipher: &[u8]) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let mut str_rails = self.decompose_cipher(cipher);

        let mut decoded = vec![];

        for i in Pendulum::new(self.rails - 1).take(cipher.len()) {
            decoded.push(str_rails[i].pop().unwrap());
        }

        Ok(decoded)
    }
}

#[test]
fn encode_test() {
    let rf = RailFence::new(4);
    debug_assert_eq!(
        rf.encode("plaintext".bytes()),
        "peltxanti".bytes().collect::<Vec<u8>>()
    )
}

#[test]
fn decode_test() -> Result<(), Box<dyn std::error::Error>> {
    let rf = RailFence::new(3);

    debug_assert_eq!(
        rf.decode(&rf.encode("plaintext".bytes()))?,
        "plaintext".bytes().collect::<Vec<u8>>()
    );

    Ok(())
}

#[test]
fn moby_dick() -> Result<(), Box<dyn std::error::Error>> {
    let rf = RailFence::new(5);

    let initial = std::fs::read_to_string("test_data/moby_dick.txt")?;

    let encoded = rf.encode(initial.bytes());
    let decoded = rf.decode(&encoded)?;

    assert!(decoded == initial.bytes().collect::<Vec<u8>>());

    Ok(())
}

#[bench]
fn bench_encode_into_decode(b: &mut test::Bencher) {
    let initial = std::fs::read_to_string("test_data/moby_dick.txt").unwrap();
    let rf = RailFence::new(5);

    b.iter(|| {
        let encoded = rf.encode(initial.bytes());
        let decoded = rf.decode(&encoded).unwrap();

        assert_eq!(decoded, initial.bytes().collect::<Vec<u8>>());
    });
}

#[bench]
fn bench_decode(b: &mut test::Bencher) {
    let initial = std::fs::read_to_string("test_data/moby_dick.txt").unwrap();
    let rf = RailFence::new(5);
    let encoded = rf.encode(initial.bytes());

    b.iter(|| {
        rf.decode(&encoded).unwrap();
    });
}
#[bench]
fn bench_encode(b: &mut test::Bencher) {
    let initial = std::fs::read_to_string("test_data/moby_dick.txt").unwrap();
    let rf = RailFence::new(5);

    b.iter(|| {
        rf.encode(initial.bytes());
    });
}

# CryptoSoft

A file encoding program. Encode with a width key, decode with that same key.

The program listens on stdin and outputs on stdout.

## Usage

There should be no change in the files between `in.txt` and `out.txt`

You can check by running `diff in.txt out.txt`.

**be careful as this will not work in powershell, since PS is UTF-16 by default, and messes with pipes. You can try with either cmd or bash**.

### Encoding

```sh
cat ./in.txt | crypto_soft -w 5 encode > encoded.txt
```

### Decoding

```sh
cat ./encoded.txt | crypto_soft -w 5 decode > out.txt
```

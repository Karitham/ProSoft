# ProSoft

3rd year C# programming project for good practices, CI/CD pipelines, testing and semver stuff

## Architecture

![Architecture](docs/Architecture/Architecture.png)

![Activity Diagram](docs/Activity%20diagram/activity%20diagram.png)

![Class Diagram](docs/Class%20diagram/class_diagram.png)

![Sequence Diagram](docs/Sequence%20diagram/sequence%20diagram.png)

![Use Case Diagram](docs/Use%20case%20diagram/use%20case%20diagram.png)

## Locations

### Daily logs

```bat
%USERPROFILE%\AppData\Local\ProSoft\EasySave\Logs\Daily.json
```

### Progress logs

```bat
%USERPROFILE%\AppData\Local\ProSoft\EasySave\Logs\Progress.json
```

### Config

```bat
%HOMEPATH%\.EasySave.json
```

using System;
using System.Collections.Generic;
using System.Linq;

namespace EasySave.Models.Process
{
    public static class Processes
    {
        public static bool IsRunning(string name)
        {
            return System.Diagnostics.Process.GetProcesses().Any(
                p => String.Equals(p.ProcessName.Replace("_", ""), name.Replace("_", ""), StringComparison.OrdinalIgnoreCase)
            );
        }
        public static bool IsRunningAny(List<string> names)
        {
            return System.Diagnostics.Process.GetProcesses().Any(
                p => names.Any(
                    n => String.Equals(p.ProcessName.Replace("_", ""), n.Replace("_", ""), StringComparison.OrdinalIgnoreCase)
                )
            );
        }
    }
}

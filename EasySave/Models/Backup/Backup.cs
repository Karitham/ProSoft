using System.Threading.Channels;
using EasySave.Models.Backup.Pipeline;
using EasySave.Models.Backup.Service;

namespace EasySave.Models.Backup
{
    public struct Backup
    {

        public BackupType Type { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public int? Encoding { get; set; }
        public string Ignore { get; set; }

        public Backup(string source, string destination, string name, BackupType type, int? encoding = null, string ignore = null)
        {
            Source = source;
            Destination = destination;
            Type = type;
            Name = name;
            Encoding = encoding;
            Ignore = ignore;
        }
        public void Produce(Channel<FileToBackup> fChan, Channel<Signal> sigChan)
        {
            new Producer(Source, Destination, Name, Ignore).ProduceFiles(fChan, Type, Encoding);
        }
    }
}
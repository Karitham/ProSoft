namespace EasySave.Models.Backup.Pipeline
{
    public enum Signal
    {
        SIGNOOP,
        SIGPAUSE,
        SIGCONT,
        SIGKILL,
        SIGSLOW,
    }
}
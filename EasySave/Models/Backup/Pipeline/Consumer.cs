using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using EasySave.Models.Logging;

namespace EasySave.Models.Backup.Pipeline
{
    class Consumer
    {
        private int _slowdown = 1000;
        private DateTime _started;
        private Mutex _muProgressState = new();
        private ProgressState _state;
        public Consumer(int slowdown = 1000)
        {
            _slowdown = slowdown;
            _started = DateTime.Now;
        }

        public async Task Consume(
           Channel<FileToBackup> backChan,
           Channel<Signal> sigChan
       )
        {
            while (true)
            {
                // Signal handling
                var signal = Signal.SIGNOOP;
                if (sigChan.Reader.TryRead(out signal) || signal != Signal.SIGNOOP)
                {

                    switch (signal)
                    {
                        case Signal.SIGPAUSE:
                            var v = await sigChan.Reader.ReadAsync();
                            if (v == Signal.SIGCONT)
                                signal = Signal.SIGNOOP;
                            else
                                Console.WriteLine($"receive unexpected signal: {v}, expected SIGCON");
                            continue;

                        case Signal.SIGKILL:
                            return;
                        case Signal.SIGSLOW:
                            Thread.Sleep(_slowdown);
                            break;
                        default:
                            Console.WriteLine("Unknown signal received");
                            break;
                    };
                }

                // TODO(@Karitham): Figure out if this can actually exit, or if we need some sort of cancellation
                var back = await backChan.Reader.ReadAsync();
                if (back == null)
                    return;

                await Run(back).ConfigureAwait(false);
            }
        }
        private Task Run(FileToBackup b) => Task.Run(() =>
             {

                 _muProgressState.WaitOne();

                 _state = new ProgressState
                 {
                     TransferedSize = _state.TransferedSize + b.Size,
                     FileProgress = _state.FileProgress + 1,
                     FileSource = b.Source,
                     FileDestination = b.Destination,
                 };

                 Progress.Instance.Log(new ProgressLog
                 {
                     State = State.Ongoing,
                     Timestamp = _started,
                     Name = b.Name,
                     ProgressState = _state,
                 });

                 _muProgressState.ReleaseMutex();

                 b.Do();
             }
        );
    }
}
using EasySave.Models.Backup.Service;

namespace EasySave.Models.Backup.Pipeline
{
    /// <summary>
    /// Backup is a class that represents a single file to backup.
    /// </summary>
    public class FileToBackup
    {
        public ISaver Saver { get; private set; }
        public string Name { get; private set; }
        public string Source { get; private set; }
        public string Destination { get; private set; }
        public int? Encoded { get; private set; }
        public float Size { get; private set; }


        public FileToBackup(string source, string destination, string name, ISaver saver, float size, int? encoded = null)
        {
            Source = source;
            Destination = destination;
            Saver = saver;
            Name = name;
            Size = size;
            Encoded = encoded;
        }

        public void Do() => this.Saver.Save(src: Source, dst: Destination);
    }
}
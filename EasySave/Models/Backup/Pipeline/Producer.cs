using System.Text.RegularExpressions;
using System.Threading.Channels;
using EasySave.Models.Backup.Service;

namespace EasySave.Models.Backup.Pipeline
{
    class Producer
    {
        private string _dst;
        private string _src;
        private string _name;
        private Regex _ignore;
        public Producer(string src, string dst, string name, string ignore = null)
        {
            _dst = dst;
            _src = src;
            _name = name;

            if (ignore != null)
            {
                _ignore = new Regex(ignore);
            }
        }

        public void ProduceFiles(Channel<FileToBackup> ch, BackupType type, int? encoding = null)
        {
            ProduceFiles(
                ch,
                Factory.CreateSaver(type, encoding),
                new System.IO.DirectoryInfo(_src),
                _dst
            );
        }

        private void ProduceFiles(Channel<FileToBackup> ch, ISaver saver, System.IO.DirectoryInfo srcRoot, string dstPrefix)
        {
            foreach (var f in srcRoot.GetFiles())
            {
                if (_ignore != null && _ignore.IsMatch(f.Name)) continue;

                //TODO(@Karitham): Figure out if we need await here
                var ftb = new FileToBackup(
                       f.FullName,
                       System.IO.Path.Combine(dstPrefix, f.Name),
                       _name,
                       saver,
                       f.Length
                    );

                ch.Writer.WriteAsync(ftb);
            }

            // Call recursively for each subdirectory.
            foreach (var dirInfo in srcRoot.GetDirectories())
            {
                string destPrefix = System.IO.Path.Combine(dstPrefix, dirInfo.Name);
                ProduceFiles(ch, saver, dirInfo, destPrefix);
            }
        }
    }
}
using System;
using System.Diagnostics;
using System.IO;
using EasySave.Utils;
using EasySave.Views.Log;

namespace EasySave.Models.Backup.Service.Impl
{
    /// <summary>
    /// Encode calls `crypo_soft.exe` to encode a file.
    /// It uses an external binary, so crypto_soft must be in path
    /// </summary>
    public class Encode : ISaver
    {

        private ISaver _saver;
        private int _width = 5;

        public BackupType Type => _saver.Type;

        public Encode(ISaver type)
        {
            _saver = type;
        }
        public Encode(ISaver type, int railWidth)
        {
            _saver = type;
            _width = railWidth;
        }

        public void Save(string src, string dst)
        {
            string new_src = NewTemp();
            encode(src: src, dst: new_src);
            _saver.Save(new_src, dst);
        }

        private void encode(string src, string dst)
        {
            ProcessStartInfo cryptoSoft = new ProcessStartInfo();

            // as such, it has to be in path somewhere
            cryptoSoft.FileName = "crypto_soft";
            cryptoSoft.Arguments = $"-w {_width} encode";

            // don't show window
            cryptoSoft.WindowStyle = ProcessWindowStyle.Hidden;
            cryptoSoft.CreateNoWindow = true;

            // make sure the encoding is right
            cryptoSoft.StandardInputEncoding = System.Text.Encoding.UTF8;
            cryptoSoft.StandardOutputEncoding = System.Text.Encoding.UTF8;
            cryptoSoft.StandardErrorEncoding = System.Text.Encoding.UTF8;

            // Since we need to write to sdin & read from stdout
            cryptoSoft.RedirectStandardInput = true;
            cryptoSoft.RedirectStandardOutput = true;
            cryptoSoft.RedirectStandardError = true;

            try
            {
                using var srcFile = new StreamReader(src);
                using var dstFile = new StreamWriter(dst);

                // Run the external process & wait for it to finish
                using (System.Diagnostics.Process proc = System.Diagnostics.Process.Start(cryptoSoft))
                {
                    Copy(src: srcFile, dst: proc.StandardInput);
                    proc.StandardInput.Close();
                    proc.WaitForExit();
                    Copy(src: proc.StandardOutput, dst: dstFile);

                    // Retrieve the app's exit code
                    if (proc.ExitCode != 0)
                    {
                        throw new System.Exception("Error while encoding");
                    }
                }
            }
            catch (Exception e)
            {
                Json.Log($"{e.Message} with source: {src} and destination: {dst}");
            }
        }

        private string NewTemp() => new TemporaryFile().FilePath;

        private void Copy(StreamReader src, TextWriter dst)
        {
            string line = null;
            while ((line = src.ReadLine()) != null)
            {
                dst.WriteLine(line);
            }
            dst.Write(src.ReadToEnd());
        }
    }
}
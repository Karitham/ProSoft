namespace EasySave.Models.Backup.Service.Impl
{
    public class CopyFile
    {
        protected void Copy(string source, string destination)
        {
            try
            {
                System.IO.File.Copy(source, destination, true);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                System.IO.Directory.GetParent(destination).Create();
            }
            finally
            {
                System.IO.File.Copy(source, destination, true);
            }
        }
    }
}
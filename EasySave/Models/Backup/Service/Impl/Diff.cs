using System;

namespace EasySave.Models.Backup.Service.Impl
{
    /// <summary>
    /// Diff is the Differential backup.
    /// It only saves the files that have changed since the last backup.
    /// This enables us to save some bandwidth since we don't need to transfer
    /// all the files over the wire to check their changes.
    /// </summary>
    internal class Diff : CopyFile, ISaver
    {
        public Diff() { }

        public BackupType Type => BackupType.Diff;

        public void Save(string src, string dst)
        {
            try
            {
                // We can literally not do anything, since the source is older, or just as old as the dst
                if (System.IO.File.GetLastWriteTimeUtc(src) <= System.IO.File.GetLastWriteTimeUtc(dst))
                {
                    return;
                }
            }
            catch (Exception)
            {
                // Noop, let's just continue
            }

            Copy(src, dst);
        }
    }
}

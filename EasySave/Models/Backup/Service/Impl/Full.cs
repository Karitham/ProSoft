namespace EasySave.Models.Backup.Service.Impl
{
    /// <summary>
    /// Full represents the full backup type.
    /// It always saves everything.
    /// </summary>
    internal class Full : CopyFile, ISaver
    {
        public Full() { }

        public BackupType Type => BackupType.Full;

        public void Save(string source, string destination)
        {
            Copy(source, destination);
        }

    }
}
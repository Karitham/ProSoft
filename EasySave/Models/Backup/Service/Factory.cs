using System;
using EasySave.Models.Backup.Service.Impl;

namespace EasySave.Models.Backup.Service
{
    static class Factory
    {
        public static ISaver CreateSaver(BackupType type, int? encoding = null)
        => type switch
        {
            BackupType.Full => WrapEncoding(new Full(), encoding),
            BackupType.Diff => WrapEncoding(new Diff(), encoding),
            _ => throw new ArgumentException("Invalid backup type"),
        };

        private static ISaver WrapEncoding(ISaver saver, int? encoding) =>
            encoding == null ? saver : new Encode(saver, encoding.Value);
    }
}
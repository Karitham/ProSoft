namespace EasySave.Models.Backup.Service
{
    /// <summary>
    /// IBackupType is the interface implemeted by backup types.
    /// This enables to to switch out between diff and full backups.
    /// </summary>
    public interface ISaver
    {
        BackupType Type { get; }
        void Save(string src, string dst);
    }
}
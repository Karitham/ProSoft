using System.Text.Json.Serialization;

namespace EasySave.Models.Backup.Service

{
    /// <summary>
    /// BackupType represent the type of backup to be made.
    /// It's used to create the right type of backup.
    /// </summary>
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BackupType
    {
        Full,
        Diff,
    }
}
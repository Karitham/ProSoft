using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using EasySave.Views.Log;

namespace EasySave.Models.Store
{
    internal struct Config
    {
        public List<Backup.Backup> Backups { get; set; }
        public List<string> StoppingProcesses { get; set; }
    }

    public sealed class Store
    {
        private static Config _config;

        private static string _path = System.IO.Path.Combine(
            Environment.ExpandEnvironmentVariables("%HOMEPATH%"),
            ".EasySave.json"
        );

        private static readonly Lazy<Store> lazy = new Lazy<Store>(() => new Store());
        public static Store Instance { get { return lazy.Value; } }

        private Store()
        {
            Read();
        }

        private void Read()
        {
            if (!System.IO.File.Exists(_path))
                CreateFile();

            try
            {
                var contents = File.ReadAllText(_path);
                _config = JsonSerializer.Deserialize<Config>(contents, new JsonSerializerOptions
                {

                });
            }
            catch (Exception)
            {
                Json.Log("Your config file is corrupted, we will back it up and create a new one, restart EasySave and try again.");
                File.Move("_path", $"{_path}.bak");
                CreateFile();
            }
            finally
            {
                var contents = File.ReadAllText(_path);
                _config = JsonSerializer.Deserialize<Config>(contents);
            }
        }
        private void CreateFile()
        {
            using var f = File.Create(_path);
            var v = JsonSerializer.Serialize(new Config { Backups = new List<Backup.Backup>() });
            f.Write(Encoding.ASCII.GetBytes(v));
        }

        private void Write()
        {
            var contents = JsonSerializer.Serialize(_config);
            File.WriteAllText(_path, contents);
        }

        public void SetBackups(List<Backup.Backup> backups)
        {
            _config.Backups = backups;
            Write();
        }

        public void AddBackup(Backup.Backup backup)
        {
            var i = _config.Backups.FindIndex(b => b.Name == backup.Name);
            if (i == -1)
                _config.Backups.Add(backup);
            else
                _config.Backups[i] = backup;
            Write();
        }

        public void RemoveBackup(string name)
        {
            if (name.Length < 1)
            {
                throw new ArgumentException("name must be at least 1 character long");
            }

            _config.Backups.RemoveAll(b => String.Equals(b.Name, name, StringComparison.OrdinalIgnoreCase));
            Write();
        }

        public List<string> StoppingProcesses()
        {
            return _config.StoppingProcesses;
        }

        public List<Backup.Backup> Backups(string name = null)
        {
            // var filter = new Func<Backup.Backup, bool>();

            return _config.Backups.Where(b =>
            {
                if (name == null || name == "*")
                    return true;
                if (b.Name.Contains(name, StringComparison.OrdinalIgnoreCase))
                    return true;

                return false;
            }).ToList();
        }
    }
}
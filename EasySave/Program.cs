namespace EasySave
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var cli = new ViewModel.CLI.CLI();
            cli.Invoke(args);
        }
    }
}

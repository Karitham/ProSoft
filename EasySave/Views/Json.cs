﻿using System.Text.Json;
using System.IO;

namespace EasySave.Views.Log
{
    internal class Json
    {
        private static TextWriter Writer = System.Console.Error;
        /// <summary>
        /// Json is an additional constructor in case we ever want to change the actual output
        /// </summary>
        public Json(TextWriter w)
        {
            Writer = w;
        }
        public Json() { }
        /// <summary>
        /// Log is the generic console logger.
        /// It writes JSON formatted data to the console.
        /// </summary>
        public static void Log(string msg)
        {
            Writer.Write(
                JsonSerializer.Serialize(
                    msg,
                    new JsonSerializerOptions { WriteIndented = true }
                ) + "\n"
            );
        }
    }
}

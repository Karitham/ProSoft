using System;
using System.IO;
using System.Collections.Generic;
using EasySave.Models.Backup;

namespace EasySave.Views
{
    /// <summary>
    /// View is the V of the MVVM pattern.
    /// It interacts with the user.
    /// For now it only uses the console.
    /// </summary>
    public class View
    {
        static private TextWriter Writer = Console.Out;

        /// <summary>
        /// Constructor of the View class.
        /// </summary>
        public View()
        {
        }

        /// <summary>
        /// Prints the existing backups to the console.
        /// They're formatted as best as possible.
        /// </summary>
        public static void ListBackups(List<Backup> backups)
        {
            if (backups.Count == 0)
            {
                Writer.WriteLine("No backups found.");
                return;
            }
            Writer.WriteLine("List of backups:");
            foreach (var b in backups)
            {
                var encoded = b.Encoding != null && b.Encoding != 0 ? $"\tencode width: {b.Encoding}" : "";
                Writer.WriteLine($"\t{b.Name}\tsrc: {b.Source}\tdst: {b.Destination}\ttype: {b.Type}{encoded}");
            }
        }
    }
}
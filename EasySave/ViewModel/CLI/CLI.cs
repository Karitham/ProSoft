using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using EasySave.Models.Backup;
using EasySave.Models.Backup.Pipeline;
using EasySave.Models.Backup.Service;
using EasySave.Models.Store;
using EasySave.Views.Log;

namespace EasySave.ViewModel.CLI
{
    /// <summary>
    /// Command line interface.
    /// It parses the argument and runs the right ops.
    /// </summary>
    public class CLI
    {
        private RootCommand _cli;

        /// <summary>
        /// Invoke the command line runner
        /// </summary>
        public void Invoke(string[] args)
        {
            _cli.Invoke(args);
        }

        public CLI()
        {
            _cli = new RootCommand("EasySave is a program that enables you to make saves easily.");
            _cli.AddCommand(Add());
            _cli.AddCommand(List());
            _cli.AddCommand(Do());
            _cli.AddCommand(Rm());
        }

        private static Command Add()
        {
            var add = new Command("add", "create a backup save");
            add.AddAlias("a");

            add.AddArgument(new Argument<string>(
                "name",
                description: "name of the save"
            ));
            add.AddArgument(new Argument<string>(
                "source",
                description: "Source directory"
            ));
            add.AddArgument(new Argument<string>(
                "destination",
                description: "Destination directory"
            ));
            add.AddOption(new Option<BackupType>(
                new string[] { "--type", "-t" },
                description: "Backup type",
                getDefaultValue: () => BackupType.Diff
            ));
            add.AddOption(new Option<int?>(
                new string[] { "--encoding", "-e" },
                description: "Encoding rail width",
                getDefaultValue: () => null
            ));

            add.Handler = CommandHandler.Create<string, string, string, BackupType, int?>(
                (name, source, destination, type, encoding) =>
                {
                    // Extrapolate
                    source = System.Environment.ExpandEnvironmentVariables(source);
                    destination = System.Environment.ExpandEnvironmentVariables(destination);

                    // Get full path
                    source = System.IO.Path.GetFullPath(source);
                    destination = System.IO.Path.GetFullPath(destination);

                    try
                    {
                        Store.Instance.AddBackup(new Backup(
                            source: source,
                            destination: destination,
                            name: name,
                            type: type,
                            encoding: encoding
                            )
                        );
                    }
                    catch (Exception e)
                    {
                        Json.Log(e.Message);
                    }
                }
            );

            return add;
        }
        private static Command List()
        {
            var list = new Command("list", "list existing backups");
            list.AddAlias("ls");

            list.AddOption(new Option<string>(
                new string[] { "--name", "-n" },
                description: "name of the backup",
                getDefaultValue: () => null
            ));

            list.Handler = CommandHandler.Create<BackupType, string>(
                (type, name) =>
                {
                    var backups = Store.Instance.Backups(name: name);
                    Views.View.ListBackups(backups);
                }
            );

            return list;
        }

        private static Command Rm()
        {
            var rm = new Command("remove", "remove a backup");
            rm.AddAlias("rm");

            rm.AddArgument(new Argument<string>(
                "name",
                description: "name of the backup"
            ));

            rm.Handler = CommandHandler.Create<string>(
                (name) =>
                {
                    try
                    {
                        Store.Instance.RemoveBackup(name);
                    }
                    catch (Exception e)
                    {
                        Json.Log(e.Message);
                    }
                }
            );

            return rm;
        }

        private static Command Do()
        {
            var doCmd = new Command("do", "do a backup");

            doCmd.AddArgument(new Argument<string>(
                "name",
                description: "name of the backup. Use `*` for parallel save of all backups.",
                getDefaultValue: () => "*"
            ));

            doCmd.Handler = CommandHandler.Create<string>(
                (name) =>
                {
                    try
                    {
                        var backups = Store.Instance.Backups(name);
                        if (backups.Count == 0)
                        {
                            Json.Log($"No backup found with name {name}");
                            return Task.CompletedTask;
                        }

                        var fileChan = Channel.CreateUnbounded<FileToBackup>();
                        var sigChan = Channel.CreateUnbounded<Signal>();

                        var toBeAwareOf = Store.Instance.StoppingProcesses();
                        var cancel = new CancellationTokenSource();

                        // Start the consuming pipeline
                        // We start it beforehand such that anytime a file is read, it is processed.
                        _ = Task.Run(ProcessCheck(sigChan, toBeAwareOf), cancel.Token);
                        _ = new Consumer().Consume(fileChan, sigChan);

                        // Start the producing pipeline
                        List<Task> tasks = new();
                        backups.ForEach(b => tasks.Add(Task.Run(() => b.Produce(fileChan, sigChan))));
                        Task.WaitAll(tasks.ToArray());

                        // Signal it's done
                        _ = fileChan.Writer.WriteAsync(null);
                        cancel.Cancel();
                    }
                    catch (Exception e)
                    {
                        Json.Log($"{e.Message}: {e.TargetSite}");
                        return Task.FromException(e);
                    }

                    return Task.CompletedTask;
                });

            return doCmd;
        }

        private static Func<Task> ProcessCheck(Channel<Signal> sigChan, List<string> toBeAwareOf) => async () =>
        {
            while (true)
            {
                if (EasySave.Models.Process.Processes.IsRunningAny(toBeAwareOf))
                {
                    Json.Log("Found work processes. Exiting...");
                    await sigChan.Writer.WriteAsync(Signal.SIGKILL);
                    return;
                }
                Thread.Sleep(100);
            }
        };
    }
}